<?php
    class DBCommand {
        // database variables
        private $db_conn;
        private $db_name;

        function __construct($conn, $name) {
            // set database
            $this->db_conn = $conn;
            $this->db_name = $name;

            $this->result = array();
        }

        function tables() {
            $temp = array();

            foreach($this->db_conn->query('SHOW TABLES') as $table) {
                // push table names array $tables
                array_push($temp, $table[0]);
            }

            // return result
            return (object) array('result' => $temp);
        }

        function fields($table) {
            $temp = array();

            foreach($this->db_conn->query('DESCRIBE '.$table) as $field) {
                // push array to $fields
                array_push($temp, array(
                        'name' => $field['Field'],
                        'type' => $field['Type']
                    )
                );
            }

            // return result
            return (object) array('result' => $temp);
        }

        function procedures() {
            $temp = array();

            // get database procedures
            foreach($this->db_conn->query('SHOW PROCEDURE STATUS WHERE db="'.$this->db_name.'"') as $proc) {
                array_push($temp, $proc['Name']);
            }

            // return result
            return (object) array('result' => $temp);
        }

        function functions() {
            $temp = array();

            // get database functions
            foreach($this->db_conn->query('SHOW FUNCTION STATUS WHERE db="'.$this->db_name.'"') as $func) {
                array_push($temp, $func['Name']);
            }

            // return result
            return (object) array('result' => $temp);
        }

        function triggers() {
            $temp = array();

            // get database triggers
            foreach($this->db_conn->query('SHOW TRIGGERS IN '.$this->db_name) as $trigger) {
                array_push($temp, $trigger['Trigger']);
            }

            // return result
            return (object) array('result' => $temp);
        }

        function events() {
            $temp = array();

            // get database events
            foreach($this->db_conn->query('SHOW EVENTS IN '.$this->db_name) as $event) {
                array_push($temp, $event['Name']);
            }

            // return result
            return (object) array('result' => $temp);
        }
    }
?>