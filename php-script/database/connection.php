<?php
    class DBConnection {
        // database variables
        private $db_host;
        private $db_name;
        private $db_user;
        private $db_pass;

        function __construct(array $param) {
            // set values
            $this->db_host = $param['host'];
            $this->db_name = $param['name'];
            $this->db_user = $param['user'];
            $this->db_pass = $param['pass'];
        }

        function connect() {
            try {
                // local variables
                $host = $this->db_host;
                $name = $this->db_name;
                $user = $this->db_user;
                $pass = $this->db_pass;

                // connect to database
                $conn = new PDO('mysql:host='.$host.';dbname='.$name, $user, $pass);

                // return the connection
                return (object) array(
                    'success' => true,
                    'connection' => $conn
                );
            } catch (PDOException $e) {
                return (object) array(
                    'success' => false,
                    'message' => $e->getMessage()
                );
            }
        }
    }
?>