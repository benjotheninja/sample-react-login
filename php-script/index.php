<?php
    require 'operations.php';

    // connect() parameters: db_name, db_user, db_pass
    $db1 = connect('db_sample_1')->command;
    $db2 = connect('db_sample_2')->command;

    $result = listOfTablesNotExist($db1, $db2);
    echo nl2br("List down the tables not existing for DB 1 and DB 2");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2— ".implode(', ', $result->table_1));
    echo nl2br("\nDB2 compared to DB1— ".implode(', ', $result->table_2));

    $result = listOfColumnsNotExist($db1, $db2);
    echo nl2br("\n\nFor tables that are included in both database,
        list down the columns not existing for tables of DB 1 and DB 2");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2: ");
    foreach($result->table_1 as $table) {
        echo nl2br("\n".$table['name']."— ".implode(', ', $table['fields']));
    }

    echo nl2br("\n\nDB2 compared to DB1: ");
    foreach($result->table_2 as $table) {
        echo nl2br("\n".$table['name']."— ".implode(', ', $table['fields']));
    }

    $result = listOfColumnsDifferentDifinitions($db1, $db2);
    echo nl2br("\n\nFor tables that are included in both database,
        list down the columns with different definition for tables of DB 1 and DB 2");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2: ");
    foreach($result->table_1 as $table) {
        echo nl2br("\n".$table['name']."— ".implode(', ', $table['fields']));
    }

    echo nl2br("\n\nDB2 compared to DB1: ");
    foreach($result->table_2 as $table) {
        echo nl2br("\n".$table['name']."— ".implode(', ', $table['fields']));
    }

    $result = listOfTriggerDifferences($db1, $db2);
    echo nl2br("\n\nList differences of triggers");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2— ".implode(', ', $result->table_1));
    echo nl2br("\nDB2 compared to DB1— ".implode(', ', $result->table_2));

    $result = listOfEventDifferences($db1, $db2);
    echo nl2br("\n\nList differences on available events");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2— ".implode(', ', $result->table_1));
    echo nl2br("\nDB2 compared to DB1— ".implode(', ', $result->table_2));
    
    $result = listOfProcedureDifferences($db1, $db2);
    echo nl2br("\n\nList differences on available stored procedure");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2— ".implode(', ', $result->table_1));
    echo nl2br("\nDB2 compared to DB1— ".implode(', ', $result->table_2));
    
    $result = listOfFunctionDifferences($db1, $db2);
    echo nl2br("\n\nList differences on available functions");
    echo nl2br("\n---------------------------------------------------------------------------------");
    echo nl2br("\nDB1 compared to DB2— ".implode(', ', $result->table_1));
    echo nl2br("\nDB2 compared to DB1— ".implode(', ', $result->table_2));
?>