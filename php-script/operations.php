<?php
    // import files
    require './database/connection.php';
    require './database/commands.php';
    
    function connect($name, $user = 'root', $pass = '') {
        $options = array(
            'host' => 'localhost',
            'name' => $name,
            'user' => $user,
            'pass' => $pass
        );
    
        $conn = new DBConnection($options);
        $conn = $conn->connect();
    
        if($conn->success) {
            $conn = $conn->connection;
            $cmd = new DBCommand($conn, $name);

            return (object) array(
                'success' => true,
                'command' => $cmd
            );
        }else
            return (object) array(
                'success' => false,
                'message' => $conn->message
            );
    }

    function listOfTablesNotExist($db1, $db2) {
        $source1 = $db1->tables()->result;
        $source2 = $db2->tables()->result;

        $comp_source1 = array();
        $comp_source2 = array();

        // compare
        foreach($source1 as $table) {
            if(!in_array($table, $source2))
                array_push($comp_source1, $table);
        }

        foreach($source2 as $table) {
            if(!in_array($table, $source1))
                array_push($comp_source2, $table);
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }

    function listOfColumnsNotExist($db1, $db2) {
        $source1 = $db1->tables()->result;
        $source2 = $db2->tables()->result;

        $common_tables = array();

        foreach($source1 as $table) {
            if(in_array($table, $source2))
                array_push($common_tables, $table);
        }

        $table1 = array();
        $table2 = array();

        foreach($common_tables as $table) {
            $temp = array();
            foreach($db1->fields($table)->result as $field) {
                array_push($temp, $field['name']);
            }

            array_push($table1, array(
                'name' => $table,
                'fields' => $temp
            ));
        }

        foreach($common_tables as $table) {
            $temp = array();
            foreach($db2->fields($table)->result as $field) {
                array_push($temp, $field['name']);
            }

            array_push($table2, array(
                'name' => $table,
                'fields' => $temp
            ));
        }

        $comp_source1 = $table1;
        $comp_source2 = $table2;

        // compare
        foreach($comp_source1 as $key => &$table) {
            $temp = array();
            foreach($table['fields'] as $field) {
                if(!in_array($field, $table2[$key]['fields']))
                    array_push($temp, $field);
            }

            $table['fields'] = $temp;
        }

        foreach($comp_source2 as $key => &$table) {
            $temp = array();
            foreach($table['fields'] as $field) {
                if(!in_array($field, $table1[$key]['fields']))
                    array_push($temp, $field);
            }

            $table['fields'] = $temp;
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }

    function listOfColumnsDifferentDifinitions($db1, $db2) {
        $source1 = $db1->tables()->result;
        $source2 = $db2->tables()->result;

        $common_tables = array();

        foreach($source1 as $table) {
            if(in_array($table, $source2))
                array_push($common_tables, $table);
        }

        $table1 = array();
        $table2 = array();

        foreach($common_tables as $table) {
            $temp = array();
            foreach($db1->fields($table)->result as $field) {
                array_push($temp, $field['name'].':'.$field['type']);
            }

            array_push($table1, array(
                'name' => $table,
                'fields' => $temp
            ));
        }

        foreach($common_tables as $table) {
            $temp = array();
            foreach($db2->fields($table)->result as $field) {
                array_push($temp, $field['name'].':'.$field['type']);
            }

            array_push($table2, array(
                'name' => $table,
                'fields' => $temp
            ));
        }

        $comp_source1 = $table1;
        $comp_source2 = $table2;

        // compare
        foreach($comp_source1 as $key => &$table) {
            $temp = array();
            foreach($table['fields'] as $field) {
                if(!in_array($field, $table2[$key]['fields']))
                    array_push($temp, $field);
            }

            $table['fields'] = $temp;
        }

        foreach($comp_source2 as $key => &$table) {
            $temp = array();
            foreach($table['fields'] as $field) {
                if(!in_array($field, $table1[$key]['fields']))
                    array_push($temp, $field);
            }

            $table['fields'] = $temp;
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }

    function listOfTriggerDifferences($db1, $db2) {
        $source1 = $db1->triggers()->result;
        $source2 = $db2->triggers()->result;

        $comp_source1 = array();
        $comp_source2 = array();
        
        foreach($source1 as $trigger) {
            if(!in_array($trigger, $source2))
                array_push($comp_source1, $trigger);
        }

        foreach($source2 as $trigger) {
            if(!in_array($trigger, $source1))
                array_push($comp_source2, $trigger);
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }

    function listOfEventDifferences($db1, $db2) {
        $source1 = $db1->events()->result;
        $source2 = $db2->events()->result;

        $comp_source1 = array();
        $comp_source2 = array();
        
        foreach($source1 as $event) {
            if(!in_array($event, $source2))
                array_push($comp_source1, $event);
        }

        foreach($source2 as $event) {
            if(!in_array($event, $source1))
                array_push($comp_source2, $event);
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }

    function listOfProcedureDifferences($db1, $db2) {
        $source1 = $db1->procedures()->result;
        $source2 = $db2->procedures()->result;

        $comp_source1 = array();
        $comp_source2 = array();
        
        foreach($source1 as $procedure) {
            if(!in_array($procedure, $source2))
                array_push($comp_source1, $procedure);
        }

        foreach($source2 as $procedure) {
            if(!in_array($procedure, $source1))
                array_push($comp_source2, $procedure);
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }

    function listOfFunctionDifferences($db1, $db2) {
        $source1 = $db1->functions()->result;
        $source2 = $db2->functions()->result;

        $comp_source1 = array();
        $comp_source2 = array();
        
        foreach($source1 as $func) {
            // if(!in_array($func, $source2))
                array_push($comp_source1, $func);
        }

        foreach($source2 as $func) {
            // if(!in_array($func, $source1))
                array_push($comp_source2, $func);
        }

        return (object) array(
            'table_1' => $comp_source1,
            'table_2' => $comp_source2
        );
    }
?>