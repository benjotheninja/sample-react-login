import { useEffect, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { AiOutlineHome, AiOutlineArrowRight} from "react-icons/ai"
import { FiTrash } from "react-icons/fi"

import InputGroup from '../components/input/InputGroup'
import InputUser from '../components/input/InputUser'
import InputPass from '../components/input/InputPass'

function Login(props) {
    // state constants
    const [user, setUser] = useState('')
    const [pass, setPass] = useState('')
    const [passVisible, setPassVisible] = useState(false)
    const [hintVisible, setHintVisible] = useState(false)
    const [deleteVisible, setDeleteVisible] = useState(false)
    const [loggedSession, setLoggedSession] = useState([])

    // router constants
    const history = useHistory()

    // componentDidMount
    useEffect(() => {
        if('user' in props.loggedUser)
            history.push('/dashboard')

        // set initial values
        setLoggedSession(props.loggedSession)
    }, [])

    const populateLoggedSession = () => {
        if(loggedSession.length) {
            return loggedSession.map(account => {
                return (
                    <div className="d-flex mb-1">
                        <span className="hvr-backward col btn btn-primary text-start text-capitalize rounded border px-3 py-2"
                            onClick={ e => handleSubmit(e, account.user, account.pass) }>
                            {account.firstname} {account.lastname}
                        </span>
                        
                        <span className={`hvr-icon-rotate btn btn-danger ms-1 ${!deleteVisible && 'd-none'}`}
                            onClick={ e => removeUserSession(account.user) }
                            >
                            <FiTrash className="hvr-icon"/>
                        </span>
                    </div>
                )
            })
        }
    }

    const removeUserSession = accountUser => {
        const sessions = [...loggedSession]
        const index = sessions.findIndex(account => account.user === accountUser)
        sessions.splice(index, 1)

        // set component state for real-time ui
        setLoggedSession(sessions)
        setDeleteVisible(false)

        // update parent component state
        props.setLoggedSession(sessions)
    }

    const handleSubmit = (e, paramUser = user, paramPass = pass) => {
        e.preventDefault()

        const sampleUser = [
            {
                user: 'danny',
                pass: 'pass',
                firstname: 'danny',
                lastname: 'rogers'
            },
            {
                user: 'joseph',
                pass: 'pass',
                firstname: 'joseph',
                lastname: 'english'
            },
            {
                user: 'james',
                pass: 'pass',
                firstname: 'james',
                lastname: 'malatabon'
            },
            {
                user: 'guest',
                pass: 'pass',
                firstname: 'guest',
                lastname: 'user'
            }
        ]

        const userAccount = sampleUser.find(account => account.user === paramUser && account.pass === paramPass)
        if(userAccount) {
            if('setLoggedUser' in props) {
                props.setLoggedUser(userAccount)
            }

            let userExist = loggedSession.find(account => account.user === userAccount.user)
            if(!userExist)
                loggedSession.push(userAccount)

            history.push('/dashboard')
        }else
            setHintVisible(true)
    }

    const handleUserEnter = bool => {
        if(bool)
            setPassVisible(true)
    }

    return (
        <section className="login">
            <div className="container">
                <div className="row align-content-center justify-content-center vh-100">
                    <form
                        className="col-md-5 bg-white shadow-sm py-3 p-md-5"
                        onSubmit={handleSubmit}>
                        <h1 className="display-4 text-center text-primary mb-0">Login</h1>
                        <p className="lead text-center mb-4">to continue using Dashboard</p>

                        <InputGroup className="border border-primary rounded">
                            <InputUser
                                color="primary"
                                value={setUser}
                                onEnter={handleUserEnter}
                                placeholder="Username"
                                required/>

                            <InputPass
                                className={`${!passVisible && 'd-none'}`}
                                color="primary"
                                value={setPass}
                                placeholder="Password"
                                required/>
                        </InputGroup>

                        <div className={`hint mt-2 ${!hintVisible && 'd-none'}`}>
                            <span className="d-flex align-items-center justify-content-center" style={{
                                fontSize: "12px"
                            }}>
                                <span className="rounded bg-warning text-white px-2 me-2"
                                    style={{
                                        fontSize: "12px"
                                    }}>
                                    HINT
                                </span>
                                user: danny/joseph/james/guest, pass: pass
                            </span>
                        </div>

                        <div className={`user-sessions row g-0 ${!(loggedSession.length) && 'd-none'}`}>
                            <small className="d-flex text-uppercase text-secondary pb-2 mt-4 mt-2">
                                <span className="me-auto">
                                    recent login
                                </span>

                                <span
                                    className="text-danger pointer"
                                    onClick={ e => setDeleteVisible(!deleteVisible) }>
                                    remove
                                </span>
                            </small>

                            {populateLoggedSession()}
                        </div>

                        <div className="controls mt-4 d-flex">
                            <Link to="/home" className="btn btn-outline-primary hvr-icon-grow me-auto">
                                <AiOutlineHome className="hvr-icon" size="20px"/>
                            </Link>

                            <button 
                                type="submit"
                                className="btn btn-outline-primary hvr-icon-forward">
                                <AiOutlineArrowRight className="hvr-icon" size="20px"/>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    )
}

export default Login