import { Link } from "react-router-dom"

function Home() {
    return (
        <section className="home">
            <div className="container">
                <div className="d-flex align-items-center vh-100">
                    <div className="wrapper text-center text-md-end">
                        <h1 className="display-1 text-primary text-center mb-0">sample-react-app</h1>
                        <p className="lead mt-1 mb-0">
                            A simple front-end implementing a "remember user" feature.
                        </p>
                        <Link className="col-12 col-md-3 btn btn-outline-primary mt-3" to="/login">
                            Login
                        </Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Home