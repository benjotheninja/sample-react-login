import { useEffect } from "react"
import { useHistory } from "react-router-dom"

function Dashboard(props) {
    const history = useHistory()

    useEffect(() => {
        if(!('user' in props.loggedUser))
            history.push('/login')
    }, [])

    return (
        <section className="dashboard">
            <div className="container">
                <div className="row py-5">
                    <div className="col-md-12 border-bottom pb-2">
                        Welcome to dashboard,
                        <span className="fs-4 text-primary text-capitalize ms-2">
                            {props.loggedUser.firstname}
                        </span>
                    </div>

                    <div className="col-md-10 offset-md-1 py-5">
                        <h3 className="fw-normal mb-3">
                            Hi, I'm James and I created this "save login" feature simulation.
                        </h3>
                        <p className="fs-5 fw-light text-secondary">
                            The saving of the information for this sample app only uses the ReactJS <code>states</code> and not <code>sessions</code> but you can easily connect this to sessions with your API or whatever you want.
                        </p>

                        <h5 className="fw-normal mt-5 mb-3">
                            Custom Components 
                            <code className="border-start border-secondary ms-2 ps-2">
                                InputGroup, InputUser, InputPass
                            </code>
                        </h5>
                        <p className="fs-5 fw-light text-secondary">
                            I created 3 custom components just for the login. Initially, I planned to follow the mockup design 100% but sadly this is the limit of my creativity so I decided do <code>button</code> type design instead of <code>select</code> type for the saved login information.
                        </p>

                        <h5 className="fw-normal mt-5 mb-3">
                            Dummy Users 
                            <code className="border-start border-secondary ms-2 ps-2">
                                Danny, Joseph, James, Guest
                            </code>
                        </h5>
                        <p className="fs-5 fw-light text-secondary">
                            In order to test the feature and see the result, I created 4 <code>users</code> for you to use in logging in. Each user has <code>user, pass, firstname, lastname</code> fields and they are located inside the <code>Login Component</code> stored in a <code>JSON</code> object named <code>sampleUser</code>.
                        </p>

                        <h5 className="fw-normal mt-5 mb-3">
                            Technologies and Tools
                            <code className="border-start border-secondary ms-2 ps-2">
                                react-icons, react-router-dom, bootstrap 5, hover.css
                            </code>
                        </h5>
                        <p className="fs-5 fw-light text-secondary">
                            Thanks to these tools that helped me make built this simple but cool app.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Dashboard