import { useState } from "react"
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai"

function InputPass(props) {
    // state constants
    const [visible, setVisible] = useState(false)

    function handleKeyPress(e) {
        const txtValue = e.target.value
        if((e.code === "Enter" || e.code === "NumpadEnter") && txtValue) {
            if('onEnter' in props)
                props.onEnter(true)

            if('value' in props)
                props.value(txtValue)
        }    
    }

    return (
        <span className={`input-pass d-flex align-items-center border-${props.color} rounded px-2 ${props.className}`}>
            <input type={`${visible? 'text': 'password'}`}
                className="element col p-2"
                onKeyPress={ e => handleKeyPress(e) }
                placeholder={props.placeholder}
                required={props.required}/>

            <AiOutlineEye
                className={`pointer text-${props.color} ${visible && 'd-none'}`}
                size="25px"
                onClick={ e => setVisible(true) }/>

            <AiOutlineEyeInvisible
                className={`pointer text-${props.color} ${!visible && 'd-none'}`}
                size="25px"
                onClick={ e => setVisible(false) }/>
        </span>
    )
}

export default InputPass