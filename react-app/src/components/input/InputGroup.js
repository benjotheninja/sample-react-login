function InputGroup(props) {
    return (
        <div className={`input-group d-flex ${props.className}`}>
            {props.children}
        </div>
    )
}

export default InputGroup