import { useState } from "react"
import { AiOutlineUser } from "react-icons/ai"

function InputUser(props) {
    // state constants
    const [active, setActive] = useState(false)

    function handleChange(e) {
        const txtValue = e.target.value
        setActive(txtValue? true: false)
    }

    function handleKeyPress(e) {
        const txtValue = e.target.value
        if((e.code === "Enter" || e.code === "NumpadEnter") && txtValue) {
            if('onEnter' in props)
                props.onEnter(true)

            if('value' in props)
                props.value(txtValue)
        }
    }

    return (
        <span className={`input-user ${active && 'input-user-active'} d-flex align-items-center border-${props.color} rounded px-2 ${props.className}`}>
            <input
                type="text"
                className="element col p-2"
                onChange={ e => handleChange(e) }
                onKeyPress={ e => handleKeyPress(e) }
                required={props.required}/>

            <AiOutlineUser className={`text-${props.color}`} size="25px"/>

            <span className="label-wrapper">
                <p className="label bg-white position-absolute rounded-pill px-2 mb-0">
                    {props.placeholder}
                </p>
            </span>
        </span>
    )
}

export default InputUser