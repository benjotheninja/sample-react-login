// components
import { Link, useHistory } from 'react-router-dom'

function Navbar(props) {
    // router constants
    const history = useHistory()
    
    function handleLogout() {
        if('onLogout' in props) {
            props.onLogout(true)
            history.push('/login')
        }
    }

    return (
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm">
            <div class="container px-md-0">
                <Link to="/" class="navbar-brand">sample-react-app</Link>
                <button class="navbar-toggler" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <Link to="/dashboard" class="nav-link">
                                Dashboard
                            </Link>
                        </li>
                    </ul>

                    <span
                        class="navbar-text text-light pointer"
                        onClick={ e => handleLogout() }>
                        Logout
                    </span>
                </div>
            </div>
        </nav>
    )
}

export default Navbar