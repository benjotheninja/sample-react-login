function Footer() {
    return (
        <footer className="footer border-top">
            <div className="container">
                <div className="d-flex justify-content-center justify-content-md-start text-secondary py-3">
                    Submitted by <b className="fw-normal text-primary ms-2">James Malatabon</b>
                </div>
            </div>
        </footer>
    )
}

export default Footer