// app styles
import 'bootstrap/dist/css/bootstrap.min.css'
import 'hover.css/css/hover-min.css'
import './css/App.css'
import './css/Component.css'

// node components
import { useState } from 'react'
import { BrowserRouter, Switch, Route, useHistory } from 'react-router-dom'

// components
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Home from './pages/Home'
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'

function App() {
    // state constants
    const [loggedUser, setLoggedUser] = useState({})
    const [loggedSession, setLoggedSession] = useState([])

    const handleLogout = isLoggedOut => {
        if(isLoggedOut)
            setLoggedUser({})
    }

    return (
        <div className="app">
            <BrowserRouter>
                <Switch>
                    <Route path="/login">
                        <Login
                            loggedUser={loggedUser}
                            loggedSession={loggedSession}
                            setLoggedSession={setLoggedSession}
                            setLoggedUser={setLoggedUser}/>
                    </Route>
                    
                    <Route path="/dashboard">
                        <Navbar
                            onLogout={handleLogout}/>

                        <Dashboard
                            loggedUser={loggedUser}/>
                            
                        <Footer/>
                    </Route>
                    
                    <Route path="/">
                        <Home/>
                        <Footer/>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    )
}

export default App